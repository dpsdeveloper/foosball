# README #

Sample project for the following problem:

```
Q3. Foosball Ratings System
At TMG, we love foosball. Most arguments over who is the best are settled on the table, but nobody can remember the scores from weeks ago. We need a way to track the results and for everyone to see who is the current king. Create a simple application that tracks foosball ratings. You should be able to input historical results, update results, and retrieve current rankings. Below are some sample historical cores to get you started.

Person,Score,Person,Score
Amos,4,Diego,5
Amos,1,Diego,5
Amos,2,Diego,5
Amos,0,Diego,5
Amos,6,Diego,5
Amos,5,Diego,2
Amos,4,Diego,0
Joel,4,Diego,5
Tim,4,Amos,5
Tim,5,Amos,2
Amos,3,Tim,5
Amos,5,Tim,3
Amos,5,Joel,4
Joel,5,Tim,2

Implement rankings view which lists users in order of their ranking (by number of games played, games won).

```



### This problem has been selected due to the following reasons: ###

* In terms of experience of utilising the latest Jetpack components.
* The problem might not be as easy as it looks for the first time
* Refresh knowledge of SQL

### Author's Precondition ###
* Jetpack navigation vs safe args
* Room as DB
* RxJava just as example, might be changed to coroutines
* ViewModels + LiveData
* Multi-modules architecture
* Dagger 
* Kotlin as primary development language


### How do I get set up? ###

* Check it out
* Refresh dependencies
* Run 
* How to run tests(No tests so far)
