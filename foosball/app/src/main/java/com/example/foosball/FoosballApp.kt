package com.example.foosball

import android.app.Application
import androidx.coordinatorlayout.widget.DirectedAcyclicGraph
import androidx.lifecycle.ProcessLifecycleOwner
import com.example.data_layer.config.DaggerDatalayerComponent
import com.example.data_layer.config.DatalayerComponent
import com.example.foosball.di.AppModule
import com.example.foosball.di.ApplicationComponent
import com.example.foosball.di.DaggerApplicationComponent
import com.example.viewmodels.configs.DaggerViewModelsComponent

/**
 * App class used for the initialization of different components.
 */
class FoosballApp : Application() {

    override fun onCreate() {
        super.onCreate()

        //Dagger global setup
       val dataLayerComponent = DaggerDatalayerComponent.builder()
            // builds once and exposes only specific dependencies
            .application(applicationContext)
            .build()

       val viewModelsComponent =  DaggerViewModelsComponent.builder()
           .datalayerComponent(dataLayerComponent)
           .build()

        installed = DaggerApplicationComponent
            .builder()
            .appModule(AppModule(applicationContext))
            .viewModelsComponent(viewModelsComponent)
            .datalayerComponent(dataLayerComponent)
            .build()

          // will delegate execution to observe for DB initialization
          installed.appObservers().forEach { ProcessLifecycleOwner.get().lifecycle.addObserver(it) }
    }

    /**
     * Just to expose appcomponent
     */
    companion object {
        private lateinit var installed: ApplicationComponent

        fun appComponent() : ApplicationComponent {
            return installed
        }
    }

}