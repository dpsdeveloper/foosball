package com.example.foosball.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.example.core.common.DiffCallback
import com.example.core.models.GameModel
import com.example.foosball.R


import com.example.foosball.screens.GamesFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.games_card_layout.view.*
import java.util.*

class GamesRecyclerViewAdapter(
     private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<GamesRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    private var mValues: List<GameModel> = Collections.emptyList()

    fun updateList(new: List<GameModel>) {
        val diff = DiffUtil.calculateDiff(DiffCallback(mValues, new))
        mValues = new
        diff.dispatchUpdatesTo(this)
    }

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as GameModel
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onItemClick(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.games_card_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        holder.leftName.text = item.winner.name
        holder.righttName.text = item.nonWinner.name
        holder.leftUserScore.text = item.winnerScope.toString()
        holder.rightUserScore.text = item.nonWinnerScore().toString()
        holder.totalScore.text = item.total.toString()

        with(holder.mView.edit_button) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val leftUserScore: TextView = mView.left_user_score
        val rightUserScore: TextView = mView.right_user_score
        val totalScore : TextView = mView.total_score_edittext
        val edit : Button = mView.edit_button
        val leftName : TextView = mView.left_name
        val righttName : TextView = mView.right_name
    }
}
