package com.example.foosball.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.core.common.DiffCallback
import com.example.core.models.UserModel
import com.example.foosball.R
import kotlinx.android.synthetic.main.user_layout.view.*
import java.util.*

class UsersRecyclerViewAdapter(private val mOnClickListener: View.OnClickListener) : RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>() {
    var mValues: List<UserModel> = Collections.emptyList()

    fun updateList(new: List<UserModel>) {
        val diff = DiffUtil.calculateDiff(DiffCallback(mValues, new))
        mValues = new
        diff.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.name.text = item.name
        if(item.surname == "UNDEFINED") {
            holder.surname.visibility = View.GONE
        }else {
            holder.surname.text = item.surname
        }
        holder.wins.text = item.wins.toString()
        holder.total.text = item.total.toString()

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val name: TextView = mView.name
        val surname: TextView = mView.surname
        val wins: TextView = mView.total_wins
        val total : TextView = mView.total_games

        override fun toString(): String {
            return super.toString() + " '" + surname.text + "'"
        }
    }


}
