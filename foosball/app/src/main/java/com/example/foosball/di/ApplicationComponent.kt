package com.example.foosball.di

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import com.example.data_layer.config.DatalayerComponent
import com.example.viewmodels.configs.FactoryProducer
import com.example.viewmodels.configs.StarUpModule
import com.example.viewmodels.configs.ViewModelsComponent
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, StarUpModule::class],
    dependencies = [DatalayerComponent::class, ViewModelsComponent::class])
interface ApplicationComponent {
    /**
     * Duplicates ViewModel component API, just for fun.
     */
    fun factoryProducer() : FactoryProducer

    /**
     * Exposes a set of Lifecycle observers specifically targeting application lifecycle.
     */
    fun appObservers() : Set<LifecycleObserver>

}

@Module
class AppModule(val context: Context) {

    @Provides
    fun appContext(): Context {
        return context
    }

    @Provides
    fun sharedPrefs(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    companion object {
        const val PREFS_NAME = "SharedPref"
    }
}