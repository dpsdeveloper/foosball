package com.example.foosball.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.foosball.FoosballApp
import com.example.foosball.R
import com.example.foosball.screens.UserSelectionFragment.Companion.SELECT_BEHAVIOUR
import com.example.viewmodels.AddGameViewModel
import com.example.viewmodels.AllUsersViewModel
import kotlinx.android.synthetic.main.add_game_layout.view.*

class AddGameFragment : Fragment() {

    private var addGame: Button? = null
    private var nonWinnerU: TextView? = null
    private var winnerU: TextView? = null
    private var winner: EditText? = null
    private var total: EditText? = null

    val viewModel : AddGameViewModel by viewModels{ FoosballApp.appComponent().factoryProducer().produce(this, bundleOf()) }

    val sharedViewModel : AllUsersViewModel by activityViewModels{ FoosballApp.appComponent().factoryProducer().produce(this, bundleOf()) }
    val args : AddGameFragmentArgs by navArgs()

    private var behaviour = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            // restores after process death
           behaviour = it.getInt(BEHAVIOR_BUNDLE_KEY,-1)
        }

        if(args.screenMode == SCREEN_MODE_EDIT){
            viewModel.editMode(args.gameId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_game_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        total = view.total_score_edit_text
        winner = view.winner_score
        winnerU = view.add_winner
        nonWinnerU = view.add_nonwineer
        addGame = view.add_game

        if(args.screenMode == SCREEN_MODE_EDIT) {
            addGame?.text = "EDIT"
        }else {
            //for this version, user selection is possible only when adding the game.
            setNavigationClick()
            setNonWinnerNavigationClick()
        }

        total?.doOnTextChanged { text: CharSequence?, _: Int, _: Int, _: Int -> viewModel.totalScoreLiveData.value = text.toString().toIntOrNull() }
        winner?.doOnTextChanged { text: CharSequence?, _: Int, _: Int, _: Int -> viewModel.winnerScoreLiveData.value = text.toString().toIntOrNull() }


        setAddGameClick()
        subscribeOnSharedUserUpdate()

    }

    private fun observeNonWinnerChange() {
        viewModel.onNonWinnerChanged().observe(viewLifecycleOwner, Observer {
            it?.let { nonWinnerU?.text = it.name }
        })
    }

    private fun observeTotalChange(){
        viewModel.totalScoreLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(total?.text.toString() != it.toString()) {
                    total?.setText(it.toString(), TextView.BufferType.EDITABLE)
                }
            }
        })
    }

    private fun observeWinnerChange(){
        viewModel.winnerScoreLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(winner?.text.toString() != it.toString()) {
                    winner?.setText(it.toString(), TextView.BufferType.EDITABLE)
                }
            }
        })
    }

    private fun observeWinnersChange() {
        viewModel.onWinnerChanged().observe(viewLifecycleOwner, Observer {
            it?.let{
                winnerU?.text = it.name
            }
        })
    }

    private fun setAddGameClick() {
        addGame?.add_game?.setOnClickListener {
            viewModel.createNewGame()
        }
    }

    private fun subscribeOnSharedUserUpdate() {
        sharedViewModel.selected.observe(viewLifecycleOwner, Observer {
            it?.let{
                // reset state
                sharedViewModel.selected.value = null
                // consumes callback and updates viewmodel
                // TODO -> think on injecting sharedViewModel with high scope into the viewmodel with lower scope
                if (behaviour == WINNER_SELECTION_BEHAVIOR) {
                    viewModel.onWinnerChanged(it)
                } else {
                    viewModel.onNonWinnerChanged(it)
                }
                behaviour = -1
            }
        })
    }

    private fun setNonWinnerNavigationClick() {
        nonWinnerU?.setOnClickListener {
            behaviour = BEHAVIOR_NONE;
            // runs selection fragment for user preselection
            val direction = AddGameFragmentDirections.actionAddGameFragmentToUserSelectionFragment(SELECT_BEHAVIOUR)
            findNavController().navigate(direction)
        }
    }

    private fun setNavigationClick() {
        winnerU?.setOnClickListener {
            // raise state of this screen to expect callback
            behaviour = WINNER_SELECTION_BEHAVIOR
            val direction = AddGameFragmentDirections.actionAddGameFragmentToUserSelectionFragment(SELECT_BEHAVIOUR)
            findNavController().navigate(direction)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // to handle process death
        outState.putInt(BEHAVIOR_BUNDLE_KEY , behaviour)
    }
    override fun onStart() {
        super.onStart()
        viewModel.onError().observe(this, Observer { if(it != null) Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show() })
        viewModel.completion.observe(this, Observer {
            if (it.isNotEmpty()) {
                viewModel.completion.value = ""
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        })

        observeWinnersChange()
        observeNonWinnerChange()
        observeTotalChange()
        observeWinnerChange()
    }
    companion object {
        const val WINNER_SELECTION_BEHAVIOR = 1;
        const val BEHAVIOR_NONE = 0;
        const val BEHAVIOR_BUNDLE_KEY = "behaviour"

        /**
         * Represents current scree operation mode, might be EDIT or ADD.
         */
        const val SCREEN_MODE_EDIT = 1;
        const val SCREEN_MODE_ADD = 0;
    }
}