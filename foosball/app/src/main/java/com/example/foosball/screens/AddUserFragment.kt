package com.example.foosball.screens

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.foosball.FoosballApp
import com.example.foosball.R
import com.example.viewmodels.AddUserViewModel


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddUserFragment : Fragment() {

    val viewModel : AddUserViewModel by viewModels { FoosballApp.appComponent().factoryProducer().produce(this, bundleOf())  }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onStart() {
        super.onStart()
        viewModel.isCompleted().observe(this , Observer { v->
            if(v) Toast.makeText(requireContext(),"Just Added",Toast.LENGTH_SHORT).show()
            closeKeyBoard()
        })
        viewModel.onError().observe(this, Observer { e ->
         Toast.makeText(requireContext(),"Failed To add",Toast.LENGTH_SHORT).show()
            closeKeyBoard()
        })
    }
    private fun closeKeyBoard(){
        val imm: InputMethodManager =
            context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = view.findViewById<EditText>(R.id.editText3)
        val surName = view.findViewById<EditText>(R.id.editText4)
        val addButton = view.findViewById<Button>(R.id.add)
        addButton.setOnClickListener {
            viewModel.addNewUser(name.text.toString(),surName.text.toString())
        }

    }
}
