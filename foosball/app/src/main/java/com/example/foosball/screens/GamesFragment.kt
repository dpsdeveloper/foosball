package com.example.foosball.screens

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.core.models.GameModel
import com.example.foosball.FoosballApp
import com.example.foosball.R
import com.example.foosball.adapters.GamesRecyclerViewAdapter
import com.example.viewmodels.JustGamesViewModel
import kotlinx.android.synthetic.main.fragment_games_list.view.*
import kotlinx.android.synthetic.main.games_card_layout.view.*

class GamesFragment : Fragment() {

    private val viewModel : JustGamesViewModel by viewModels{ FoosballApp.appComponent().factoryProducer().produce(this, bundleOf())}

    private val adapter = GamesRecyclerViewAdapter(
        object : OnListFragmentInteractionListener {
            override fun onItemClick(item: GameModel?) {
                // require game id to pass alongside the direction
               val direction = GamesFragmentDirections.actionGamesFragmentToAddGameFragment(AddGameFragment.SCREEN_MODE_EDIT, item?.id ?:0 )
                // create direction attributes, and pass them through the stack
                findNavController().navigate(direction)
            }
        } )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_games_list, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        recyclerView?.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.floatingActionButton.setOnClickListener{
            findNavController().navigate(R.id.action_gamesFragment_to_addGameFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onError().observe(this, Observer { Toast.makeText(requireContext(),"Something Went Wrong",Toast.LENGTH_SHORT).show() })
        viewModel.allGamesResult().observe(this, Observer { adapter.updateList(it) })
        viewModel.allGames()
    }

    /**
     * Callback on the selected model in Recycler
     */
    interface OnListFragmentInteractionListener {
        fun onItemClick(item: GameModel?)
    }

}
