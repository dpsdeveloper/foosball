package com.example.foosball.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.foosball.R

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.users).setOnClickListener {
            findNavController().navigate(R.id.action_add_user)
        }

        view.findViewById<Button>(R.id.ratins).setOnClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToUserSelectionFragment(UserSelectionFragment.VIEW_BEHAVIOUR)
            findNavController().navigate(direction)
        }

        view.findViewById<Button>(R.id.games).setOnClickListener {
            findNavController().navigate(R.id.action_MainFragment_to_gamesFragment)
        }
    }
}
