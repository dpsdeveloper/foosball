package com.example.foosball.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.models.UserModel
import com.example.foosball.FoosballApp
import com.example.foosball.R
import com.example.foosball.adapters.UsersRecyclerViewAdapter
import com.example.viewmodels.AllUsersViewModel
import com.example.viewmodels.AllUsersViewModel.Companion.SORT_BY_TOTAL
import com.example.viewmodels.AllUsersViewModel.Companion.SORT_BY_WINS
import kotlinx.android.synthetic.main.fragment_user_selection_list.view.*


class UserSelectionFragment : Fragment() {

    val args : UserSelectionFragmentArgs by navArgs()

    val viewModel : AllUsersViewModel by activityViewModels{ FoosballApp.appComponent().factoryProducer().produce(this, bundleOf()) }

    private var adapter = UsersRecyclerViewAdapter(View.OnClickListener {
        val model = it.tag as? UserModel
        viewModel.selected(model)
        // selected spinner behaviour
        if(args.behaviour == SELECT_BEHAVIOUR) {
            // once selected, navigate back immediately.
            findNavController().popBackStack()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // options selection is used for sort options
        if(args.behaviour == VIEW_BEHAVIOUR) {
            setHasOptionsMenu(true)
        }
    }
    override fun onStart() {
        super.onStart()
        viewModel.users.observe(this, Observer{list -> adapter.updateList(list) })
        // request all users from Data layer
        viewModel.requestUsers()

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_selection_list, container, false)
        val recycler = view.list
        recycler.adapter = adapter
        recycler.addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.total -> viewModel.requestUsers(SORT_BY_TOTAL).let { true }
            R.id.wins -> viewModel.requestUsers(SORT_BY_WINS).let { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object{
        /**
         * Used for force this fragment interact as a spinner for user selection
         */
        const val SELECT_BEHAVIOUR = 1
        const val VIEW_BEHAVIOUR = 2
    }
}
