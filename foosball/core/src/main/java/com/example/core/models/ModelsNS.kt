package com.example.core.models

/**
 * Data classes to be shared between different modules of the app.
 * Primary goal of this kind of clases is to isolate details implementation from the `clients` code.
 * As example, app doesn't need to be dependent on any specific Database.
 */
data class UserModel(val name: String, val surname: String, val id: Long, val wins : Int, val total : Int) {}

data class GameModel(val winner: UserModel, val nonWinner: UserModel, val total: Int, val winnerScope: Int, val id: Long) {
    fun nonWinnerScore() : Int {
        return total - winnerScope
    }
}


