####  Data layer module, in charge for the following functionality: ####
1. Maintain DB invariants and integrity
2. Merge different datasources of data into the single one
3. Retain Entity classes.

 Should have the following properties:

#### Must exposes single interface for communication. ####
#### Must exposes child component to make integration simple and hide details ####


#### Exposes the following entyties and interfaces ####

*  `User` - user related data.
*  `Game` - user to user relationship with game associated info.
*  `GameWithUsers` - pre-populated data with Users and Game info.
*  `GameRepository` - interface for maintain Game's related operations
*  `UserRepository` - interface for maintain User's related operations

