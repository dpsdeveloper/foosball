package com.example.data_layer.config

import android.app.Application
import android.content.Context
import com.example.data_layer.dao.GameDao
import com.example.data_layer.dao.UserDao
import com.example.data_layer.db.FoosBallDatabase
import com.example.data_layer.repositories.GamePreconditionsValidationDecorator
import com.example.data_layer.repositories.GamesRepository
import com.example.data_layer.repositories.RoomGameUserUpdateRepositoryDecorator
import com.example.data_layer.repositories.RoomGamesRepository
import com.example.data_layer.repositories.RoomUserRepository
import com.example.data_layer.repositories.UserRepository
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
annotation class DataScope

@DataScope
@Component(modules = [DatabaseModule::class])
interface DatalayerComponent {
    /**
     * Property to expose from this component
     */
    fun userRepository(): UserRepository
    /**
     * Property to expose from this component
     */
    fun gameRepository(): GamesRepository

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(context: Context): Builder
        fun build(): DatalayerComponent
    }
}

@Module
class DatabaseModule {

    @Provides
    @DataScope
    fun config(context: Context): FoosBallDatabase {
        return FoosBallDatabase.build(context)
    }

    @Provides
    @DataScope
    fun bindUserDao(db: FoosBallDatabase): UserDao {
        return db.userDao()
    }

    @Provides
    @DataScope
    fun bindGameDao(db: FoosBallDatabase): GameDao {
        return db.gameDao()
    }

    @Provides
    fun createUserRepo (userDao: UserDao): UserRepository {
        return RoomUserRepository(userDao)
    }

    @Provides
    fun createGameRepo (gameDao: GameDao, userRepository: UserRepository): GamesRepository {
        return RoomGameUserUpdateRepositoryDecorator( GamePreconditionsValidationDecorator( RoomGamesRepository(gameDao)),userRepository)
    }

}
