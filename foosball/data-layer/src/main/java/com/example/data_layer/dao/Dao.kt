package com.example.data_layer.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.data_layer.entity.Game
import com.example.data_layer.entity.GameWithUsers
import com.example.data_layer.entity.User
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface UserDao {
    /**
     * Returns all users in the database.
     */
    @Query("SELECT * FROM users ORDER BY totalGames DESC")
    fun allUsers(): Single<List<User>>

    /**
     * Returns all users in the database.
     */
    @Query("SELECT * FROM users ORDER BY totalWins DESC")
    fun allUsersSortByWins(): Single<List<User>>

    /**
     * Adds or replace existed user
     */
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun addUser(user: User): Completable

    /**
     * Updates total games and total wins
     */
    @Query("UPDATE users SET totalGames = totalGames + 1 , totalWins = totalWins + 1 WHERE userId = :id")
    fun updateWinnerWithId(id : Long) : Completable

    /**
     * Updates just total games
     */
    @Query("UPDATE users SET totalGames = totalGames + 1  WHERE userId = :id")
    fun updateNonWinnerWithId(id : Long) : Completable

    /**
     * Bulk insert of users
     */
    @Insert
    fun insertAll(users: List<User>) : Single<List<Long>>
}

@Dao
interface GameDao {
    /**
     * Adds new game to the Database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addOrUpdateGame(game: Game): Completable

    /**
     * Returns all games in the database.
     */
    @Query("SELECT * FROM games ")
    fun allGames(): Single<List<Game>>


    /**
     * Returns game with users by it
     */
    @Query("SELECT * FROM games WHERE gameId == :id")
    fun gameWithId(id : Long): Single<GameWithUsers>

    /**
     *  Transaction - required since ROOM makes two queries.
     *  Returns list of all users.
     */
    @Transaction
    @Query("SELECT * FROM games")
    fun allGamesWithUsers(): Single<List<GameWithUsers>>
}