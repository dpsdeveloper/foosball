package com.example.data_layer.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.data_layer.dao.GameDao
import com.example.data_layer.dao.UserDao
import com.example.data_layer.entity.Game
import com.example.data_layer.entity.User

@Database(entities = [User::class, Game::class], version = 1, exportSchema = false)
abstract class FoosBallDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun gameDao(): GameDao

    companion object {
        const val DB_NAME = "foosball.db"

        fun build(context: Context) = Room.databaseBuilder(context.applicationContext, FoosBallDatabase::class.java, DB_NAME).build()
    }
}