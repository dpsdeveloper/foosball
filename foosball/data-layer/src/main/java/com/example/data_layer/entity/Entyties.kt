package com.example.data_layer.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.lang.IllegalArgumentException

@Entity(tableName = "users",indices = [Index("name"),Index("surname")])
data class User(
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "surname") val surname: String,
    @ColumnInfo(name = "totalGames") val totalGames: Int = 0,
    @ColumnInfo(name = "totalWins") val totalWins: Int = 0
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "userId")
    var userId: Long = 0
}

/**
 * Games table
 */
@Entity(tableName = "games",
    foreignKeys = [
    ForeignKey(entity = User::class, parentColumns = ["userId"], childColumns = ["winnerId"]),
    ForeignKey(entity = User::class, parentColumns = ["userId"], childColumns = ["nonWinnerId"])],
    indices = [Index("nonWinnerId"),Index("winnerId")]
)
data class Game(
    @ColumnInfo(name = "total")
    val totalScore : Int,
    @ColumnInfo(name = "winners")
    val winnerScore : Int,
    @ColumnInfo(name = "winnerId")
    val winnerId : Long,
    @ColumnInfo(name = "nonWinnerId")
    val nonWinnerId : Long,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "gameId")
    var gameId: Long = 0
) {
}

data class GameWithUsers(
    @Embedded val game: Game,
    @Relation(parentColumn = "winnerId", entityColumn = "userId") var winner: User,
    @Relation(parentColumn = "nonWinnerId", entityColumn = "userId") var nonWinner: User
) {}
