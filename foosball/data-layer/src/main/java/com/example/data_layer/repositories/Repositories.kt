package com.example.data_layer.repositories

import android.util.Log
import com.example.data_layer.dao.GameDao
import com.example.data_layer.dao.UserDao
import com.example.data_layer.entity.Game
import com.example.data_layer.entity.GameWithUsers
import com.example.data_layer.entity.User
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.IllegalArgumentException

/**
 * High level interface for operate onto User's entities
 */
interface UserRepository {

    fun allUsers(): Single<List<User>>

    fun allUsersSortByWins(): Single<List<User>>

    fun createUser(name: String, surname: String) : Completable

    fun updateWinnerWithId(id : Long) : Completable

    fun updateNonWinner(id:Long) : Completable

    fun bulkUserCreate(names : Collection<String>) : Single<List<Long>>
}

/**
 * DB based repo for User table.
 */
class RoomUserRepository(val userDao: UserDao) : UserRepository {

    override fun allUsers(): Single<List<User>> = userDao.allUsers()
    override fun allUsersSortByWins(): Single<List<User>> = userDao.allUsersSortByWins()
    override fun createUser(name: String, surname: String) = userDao.addUser(User(name, surname))
    override fun updateWinnerWithId(id: Long) = userDao.updateWinnerWithId(id)
    override fun updateNonWinner(id: Long)  = userDao.updateNonWinnerWithId(id)
    override fun bulkUserCreate(names : Collection<String> ): Single<List<Long>> = names.map { User(it,"UNDEFINED") }.toList().let { userDao.insertAll(it) }
}

/**
 * High level interface for games entities.
 */
interface GamesRepository {
    /**
     * Creates new game
     */
    fun createNewOrUpdate(totalScore: Int, winnerScore: Int, winner: Long, nonWinner: Long, id: Long = 0): Completable

    /**
     * Returns all games in the system
     */
    fun allGames(): Single<List<Game>>

    fun allGamesWithAssignedUsers() : Single<List<GameWithUsers>>

    fun getGameById(id : Long) : Single<GameWithUsers>
}

/**
 * Validation decorator, is in charge of watching preconditions of the data.
 */
class GamePreconditionsValidationDecorator ( private val delegate : GamesRepository): GamesRepository by delegate{
    override fun createNewOrUpdate(totalScore: Int, winnerScore: Int, winner: Long, nonWinner: Long, id: Long): Completable {
        // preconditions
        if(totalScore < winnerScore) return Completable.error(IllegalArgumentException(" Wrong data assertion total:$totalScore<winnerScore:$winner "))
        // updating case
        if(id > 0){
            // winner score + 1 has to being greater then total / 2 + 1
            if(winnerScore < (totalScore/2) + 1 ) return Completable.error(IllegalArgumentException("Wrong data assertion winner score has to be greater then half of total "))
        }
        return delegate.createNewOrUpdate(totalScore, winnerScore, winner, nonWinner, id)
    }
}

/**
 *  Designed for consecutive update of total and wins field of the user entity, more optimal to add DB trigger.
 */
class RoomGameUserUpdateRepositoryDecorator(private val delegate : GamesRepository, private val userRepo : UserRepository) : GamesRepository by delegate{
    override fun createNewOrUpdate(totalScore: Int, winnerScore: Int, winner: Long, nonWinner: Long, id: Long): Completable {
        // in a case of update, the following conditions have to be met:
        // total has to be les then winner score
        // don't need to run update user logic
        // in fact only scores might being updated.
        return delegate.createNewOrUpdate(totalScore, winnerScore, winner, nonWinner, id)
             // runs queries for updating required fields
            .andThen(
            if(id > 0){
                // just to avoid DB integrity violation, first version of the app is not updating
                // total and winner score while editing game.
                Completable.complete()
            } else {
                // +1 to winner score and total
                userRepo.updateWinnerWithId(winner)
                     // +1 to total only
                    .andThen(userRepo.updateNonWinner(nonWinner))
            })

    }
}


/**
 * DB based repo for Game table.
 */
class RoomGamesRepository(val gameDao: GameDao) : GamesRepository {
    /**
     * Creates new Game and assigns Users
     */
    override fun createNewOrUpdate(totalScore: Int, winnerScore: Int, winner: Long, nonWinner: Long, id: Long): Completable
            = gameDao.addOrUpdateGame(Game(totalScore, winnerScore, winner, nonWinner,id))

    /**
     * Returns just all games
     */
    override fun allGames() = gameDao.allGames()

    /**
     * Returns all games plus their users.
     */
    override fun allGamesWithAssignedUsers(): Single<List<GameWithUsers>> = gameDao.allGamesWithUsers()

    override fun getGameById(id: Long): Single<GameWithUsers>  = gameDao.gameWithId(id)

}