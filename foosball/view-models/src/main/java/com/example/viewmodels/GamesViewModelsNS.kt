package com.example.viewmodels

import android.util.Log
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.core.models.GameModel
import com.example.core.models.UserModel
import com.example.data_layer.entity.GameWithUsers
import com.example.data_layer.entity.User
import com.example.data_layer.repositories.GamesRepository
import com.example.data_layer.repositories.UserRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalArgumentException

/**
 * Add Game viewmodel, is in charge of the following:
 * 1. Insert new game entity to the database.
 * 2. Notify clients on the progress of action execution
 * 3. Notify clients on the completion of the action.
 */
class AddGameViewModel(private val gamesRepository: GamesRepository,
                       val handle: SavedStateHandle
) : ViewModel(){

    init {

        // TODO -> add Process state restoration for the selected users so far when viewmodel is in the edit mode.
        // TODO -> as example of SavedStateHandle integration.
        handle.get<String?>("updated")?.let {
            Log.d("AddGameViewModel","State restored")
        }
    }
    private val bag = CompositeDisposable()

    private val progress = MutableLiveData<Boolean>()

    private val winner = MutableLiveData<UserModel>()

    private val nonWinner = MutableLiveData<UserModel>()

    val totalScoreLiveData = MutableLiveData<Int>()

    val winnerScoreLiveData = MutableLiveData<Int>()

    val completion = MutableLiveData<String>()

    private val error = MutableLiveData<Throwable>()

    private var gameId : Long = 0

    /**
     * Indicates whether or not current viewmodel already restored self state
     */
    var restored : Disposable? = null

    /**
     * In a case of errors
     */
    fun onError() : LiveData<Throwable> = error

    /**
     * For observing winners change
     */
    fun onWinnerChanged() : LiveData<UserModel> = winner

    /**
     * For observing non winners
     */
    fun onNonWinnerChanged() : LiveData<UserModel> = nonWinner

    /**
     * Once user collected required fields, it has to invoke this method.
     */
    fun createNewGame() {
        val winnerScore = winnerScoreLiveData.value ?: 0
        val totalScore = totalScoreLiveData.value ?: 0
        // total and winner need to be validated since user can modify them and screen doesn't have

        val winnerId = winner.value?.id ?: -1
        val nonWinnerId = nonWinner.value?.id ?: -1

        // validating for user equality, game with two identical users can't be created.
        if(winnerId == nonWinnerId || winnerId < 0 || nonWinnerId < 0){
            error.value = IllegalArgumentException("You cant select same user twice")
            return
        }

        bag.clear()
        bag.addAll(
            gamesRepository.createNewOrUpdate(totalScore, winnerScore ,winnerId, nonWinnerId,gameId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progress.value = true }
                .doFinally{progress.value = false}
                .subscribe({ completion.value = "Just created/updated" },
                    { error.value = it })
        )
    }

    override fun onCleared() {
        super.onCleared()
        bag.clear()
    }

    @MainThread
    fun onWinnerChanged(model: UserModel) {
        winner.value = model

    }

    @MainThread
    fun onNonWinnerChanged(model: UserModel) {
        nonWinner.value = model
        // just as an exmaple of saving state to handle
        handle.set("updated","updated")
    }

    fun editMode(gameId: Long) {
    if(restored != null) return
      this.gameId = gameId
      restored =  gamesRepository.getGameById(gameId)
           .map { it.convert()}
           .subscribeOn(Schedulers.io())
           .observeOn(AndroidSchedulers.mainThread())
           .doOnSuccess { winner.value = it.winner }
           .doOnSuccess { nonWinner.value = it.nonWinner }
           .doOnSuccess { totalScoreLiveData.value = it.total }
           .doOnSuccess { winnerScoreLiveData.value = it.winnerScope }
           .subscribe({ completion.value = "Data restored from DB" },{ error.value = it })
    }
}

/**
 * Viewmodel designed for representing list of games with users assigned.
 */
class JustGamesViewModel(private val gamesRepository: GamesRepository) : ViewModel(){
    private val bag = CompositeDisposable()

    private   val allGamesLiveData = MutableLiveData<List<GameModel>>()
    private val error = MutableLiveData<Throwable>()

    fun onError() : LiveData<Throwable> = error

    fun allGamesResult() : LiveData<List<GameModel>> = allGamesLiveData

    /**
     * Just returns all list of games with user assigned
     */
    fun allGames(){
    bag.clear()
    val disposable =    gamesRepository.allGamesWithAssignedUsers()
            .flatMap {list -> Observable.fromIterable(list).map {
                // maps entity to model class
                it.convert()
            }.toList()}
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { bag.clear() }.subscribe({ allGamesLiveData.value = it}, { e -> error.value = e})
    bag.addAll(disposable)
    }
}

/**
 * Converts Entity class to Model.
 */
private fun GameWithUsers.convert() : GameModel{
  return  GameModel(
        this.winner.let { UserModel(it.name,it.surname,it.userId,it.totalWins,it.totalGames) },
        this.nonWinner.let { UserModel(it.name,it.surname,it.userId,it.totalWins,it.totalGames)},
        this.game.totalScore,this.game.winnerScore,this.game.gameId)
}