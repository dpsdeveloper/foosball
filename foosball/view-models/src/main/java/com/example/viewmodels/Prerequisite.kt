package com.example.viewmodels

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.data_layer.repositories.GamesRepository
import com.example.data_layer.repositories.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Based on the task, DB has to be prepopulate with data.
 */
class Prerequisite @Inject constructor(val gamesRepository: GamesRepository,
                   val context: Context ,
                   val userRepository: UserRepository,
                   val preferences: SharedPreferences) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        if(preferences.contains(KEY)){
            return
        }
        val disposable = Single.fromCallable {
            val lines = context.resources.openRawResource(R.raw.example).bufferedReader().readLines()
            val uniqueUsers = mutableSetOf<String>()
            //skip first
            for (i in 1 until lines.size) {
                val content = lines[i].split(",")
                // 1, 3 names
                uniqueUsers.add(content[0])
                uniqueUsers.add(content[2])
            }
            Pair(uniqueUsers.toList(), lines)
        }.flatMap { pair ->
            // batch insert to the users table
            userRepository.bulkUserCreate(pair.first)
                .map {roomResponse ->
                    // maps names to user's entity id
                    val map = mutableMapOf<String,Long>()
                    pair.first.forEachIndexed{i,v -> map[v] = roomResponse[i] }
                   // Name to DB ID
                   Pair(map,pair.second.subList(1,pair.second.size))
                }
        }
            .flatMapCompletable { pair ->
               Observable.fromIterable(pair.second).map { it.split(",") }.flatMapCompletable {
               val left = it[1].toInt()
               val right = it[3].toInt()
               val total = left + right
               val winnerScore = left.coerceAtLeast(right)
               val winnerName = if(winnerScore == left) it[0] else it[2]
               val nonWinnerName = if(winnerName == it[0]) it[2] else it[0]
              return@flatMapCompletable  gamesRepository.createNewOrUpdate(total,winnerScore,pair.first[winnerName] ?: 0,pair.first[nonWinnerName] ?: 0)
            }
        }.doOnComplete {
                // disable next app update
                preferences.edit().putBoolean(KEY,true).apply() }
            .subscribeOn(Schedulers.io()).subscribe ({
                Log.d("Prerequisite", "Done")
            },{ Log.d("Prerequisite", it.message,it) })
    }
    companion object{
        const val KEY ="Prerequisite-Populated"
    }
}
