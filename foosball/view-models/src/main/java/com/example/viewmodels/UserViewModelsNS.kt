package com.example.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.core.models.UserModel
import com.example.data_layer.entity.User
import com.example.data_layer.repositories.GamesRepository
import com.example.data_layer.repositories.RoomGamesRepository
import com.example.data_layer.repositories.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Add User viewmodel, is in charge of the following:
 * 1. Insert new user to the database.
 * 2. Notify clients on the progress of action execution
 * 3. Notify clients on the completion of the action.
 */
class AddUserViewModel(private val userRepo: UserRepository,
                       handle: SavedStateHandle
) : ViewModel() {

    private val bag = CompositeDisposable()

    private val progress = MutableLiveData<Boolean>()

    private val completion = MutableLiveData<Boolean>()

    private val error = MutableLiveData<Throwable>()

    /**
     * Exposes reduced type, just LiveData
     */
    fun inProgress(): LiveData<Boolean> = progress

    /**
     * Exposes reduced type, just LiveData
     */
    fun isCompleted(): LiveData<Boolean> = completion

    fun onError() : LiveData<Throwable> = error

    /**
     * Once user collected required fields, it has to invoke this method.
     */
    fun addNewUser(name: String, surname: String) {
        // TODO -> add validation rules for the input parameters.
        bag.add(
            userRepo.createUser(name, surname)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progress.value = true }
                .doFinally { bag.clear() }.subscribe({
                    completion.value = true },
                    {
                        error.value = it
                    })
        )
    }

    override fun onCleared() {
        super.onCleared()
        bag.clear()
    }
}

/**
 * Shared activity level view model.
 */
class AllUsersViewModel(private val userRepo: UserRepository) : ViewModel() {
    private val bag = CompositeDisposable()

    /**
    * Exposes mutable LiveData, clients are in charge of reset it once consumed.
    */
    val selected = MutableLiveData<UserModel>()

    private val progress = MutableLiveData<Boolean>()

    private val error = MutableLiveData<Throwable>();

    val users = MutableLiveData<List<UserModel>>()

    private var prev : List<UserModel> = Collections.emptyList()
    /**
     *  Should be invoked for fetch users
     */
    fun requestUsers(sortBy : Int = SORT_BY_TOTAL) {
        bag.clear()
        val disposable = Single.just(sortBy).flatMap {
            if(it == SORT_BY_TOTAL){ userRepo.allUsers()}else { userRepo.allUsersSortByWins() }}
            .flatMap { users ->
                Observable
                 // maps list of Entities to list of Models
                .fromIterable(users)
                .map { UserModel(it.name,it.surname,it.userId,it.totalWins,it.totalGames) }
                .toList() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progress.value = true }
            .doFinally{progress.value = false}
            .subscribe({ users.value = it
                 },
                { error.value = it })
        bag.add(disposable)
    }

    /**
     * Once user pressed selection, it will settle on the five data field.
     */
    fun selected(model: UserModel?) {
        selected.value = model
    }

    companion object {
        const val SORT_BY_TOTAL = 0
        const val SORT_BY_WINS = 1
    }
}