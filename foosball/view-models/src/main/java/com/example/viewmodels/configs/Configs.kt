package com.example.viewmodels.configs

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.savedstate.SavedStateRegistryOwner
import com.example.data_layer.repositories.GamesRepository
import com.example.data_layer.repositories.UserRepository
import com.example.viewmodels.AddGameViewModel
import com.example.viewmodels.AddUserViewModel
import com.example.viewmodels.AllUsersViewModel
import com.example.viewmodels.JustGamesViewModel
import javax.inject.Inject

/**
 *  Is not injected since it is being created manually from the Producer.
 */
class ViewModelsFactory constructor(val userRepository: UserRepository, val gamesRepository: GamesRepository,
                                            owner: SavedStateRegistryOwner,
                                            defaultArgs: Bundle?
): AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(
        key: String, modelClass: Class<T>, handle: SavedStateHandle
    ): T {
        if (modelClass.isAssignableFrom(AllUsersViewModel::class.java)){
            return AllUsersViewModel(userRepository) as T
        }
        if(modelClass.isAssignableFrom(AddUserViewModel::class.java)) {
            return AddUserViewModel(userRepository,handle) as T
        }
        if(modelClass.isAssignableFrom(JustGamesViewModel::class.java)) {
            return JustGamesViewModel(gamesRepository) as T
        }
        if(modelClass.isAssignableFrom(AddGameViewModel::class.java)) {
            return AddGameViewModel(gamesRepository,handle) as T
        }
        throw IllegalArgumentException("Wrong configuration")
    }
}

/**
 * Example of manual "associated" injection. Might be changed with AutoFactory(Google) or library from Jake(AssociatedInjection)
 */
class Producer @Inject constructor(val userRepository: UserRepository, val gamesRepository: GamesRepository): FactoryProducer {
    override fun produce(
        owner: SavedStateRegistryOwner, defaultArgs: Bundle
    ): ViewModelProvider.Factory {
       return ViewModelsFactory(userRepository,gamesRepository,owner,defaultArgs)
    }

}

/**
 * Will be exposed outside of component. Is used to pass SDK related arguments through the stack.
 */
interface FactoryProducer {

    fun produce(owner : SavedStateRegistryOwner, defaultArgs: Bundle) : ViewModelProvider.Factory

}