package com.example.viewmodels.configs

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import com.example.data_layer.config.DatalayerComponent
import com.example.viewmodels.Prerequisite
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoSet

annotation class ViewModeScope

@ViewModeScope
@Component(modules = [InternalModule::class], dependencies = [DatalayerComponent::class])
interface ViewModelsComponent {
    /**
     * Exposes just single factory for producing viewmodels.
     */
    fun produce() : FactoryProducer

}

/**
 * Hidded inside ViewModelsComponent. Just FactoryProducer will be exposed.
 */
@Module
abstract class InternalModule {

    @Binds
    abstract fun viewModelsFactory(factory : ViewModelsFactory) : ViewModelProvider.Factory

    @Binds
    abstract fun factoryProducer(producer: Producer) : FactoryProducer
}

/**
 * Exposes Prerequisite object to the outer Dagger's graph.
 */
@Module
abstract class StarUpModule {

    @Binds
    @IntoSet
    abstract fun appPrerequisites(prerequisite: Prerequisite): LifecycleObserver
}
